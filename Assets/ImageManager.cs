﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageManager : MonoBehaviour
{
    public GameObject[] ImageTarget;

    private void Update()
    {
        for (int i = 0; i < ImageTarget.Length; i++)
        {
            Destroy(ImageTarget[i]);
        }
    }
}
