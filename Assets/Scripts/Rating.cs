﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rating : MonoBehaviour
{
    public Image[] Star;
    public int whatStar;

    public void onClickRate1Star()
    {
        Star[0].gameObject.SetActive(true);
        Star[1].gameObject.SetActive(false);
        Star[2].gameObject.SetActive(false);
        Star[3].gameObject.SetActive(false);
        Star[4].gameObject.SetActive(false);
        whatStar = 1;
    }

    public void onClickRate2Star()
    {
        Star[0].gameObject.SetActive(true);
        Star[1].gameObject.SetActive(true);
        Star[2].gameObject.SetActive(false);
        Star[3].gameObject.SetActive(false);
        Star[4].gameObject.SetActive(false);
        whatStar = 2;
    }
    public void onClickRate3Star()
    {
        Star[0].gameObject.SetActive(true);
        Star[1].gameObject.SetActive(true);
        Star[2].gameObject.SetActive(true);
        Star[3].gameObject.SetActive(false);
        Star[4].gameObject.SetActive(false);
        whatStar = 3;
    }
    public void onClickRate4Star()
    {
        Star[0].gameObject.SetActive(true);
        Star[1].gameObject.SetActive(true);
        Star[2].gameObject.SetActive(true);
        Star[3].gameObject.SetActive(true);
        Star[4].gameObject.SetActive(false);
        whatStar = 4;
    }
    public void onClickRate5Star()
    {
        Star[0].gameObject.SetActive(true);
        Star[1].gameObject.SetActive(true);
        Star[2].gameObject.SetActive(true);
        Star[3].gameObject.SetActive(true);
        Star[4].gameObject.SetActive(true);
        whatStar = 5;
    }

    public void Rate()
    {
        this.gameObject.GetComponent<Score>().numberOfRates++;
        if (whatStar == 1)
        {
            Debug.Log("your score is: " + whatStar);
            this.gameObject.GetComponent<Score>().collaboration += 1;
        }
        if (whatStar == 2)
        {
            Debug.Log("your score is: " + whatStar);
            this.gameObject.GetComponent<Score>().collaboration += 2;
        }
        if (whatStar == 3)
        {
            Debug.Log("your score is: " + whatStar);
            this.gameObject.GetComponent<Score>().collaboration += 3;
        }
        if (whatStar == 4)
        {
            Debug.Log("your score is: " + whatStar);
            this.gameObject.GetComponent<Score>().collaboration += 4;
        }
        if (whatStar == 5)
        {
            Debug.Log("your score is: " + whatStar);
            this.gameObject.GetComponent<Score>().collaboration += 5;
        }
        

    }
}
