﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionController : MonoBehaviour
{
    public MissionScript[] MissionScripts;

    public GameObject Marker;
    public GameObject RateGame;
    public GameObject MissionScreen;
    public GameObject MainScreen;
    public GameObject QR;
    public GameObject backButton;
    public GameObject InstructionsBox;
    public GameObject FinalQuestion;
    public GameObject GKCButton;
    public GameObject QuestionBox;
    public GameObject[] Tiles;

    public GameObject[] Markers;

    public Text MissionNumber;
    public Text Instructions;
    public Text Question;
    public Text GKCButtonText;
    public Text Hint;
    public Text Result;

    public InputField AnswerBox;

    public int CurrentMission;

    private int GKCCounter;

    private void Start()
    {
        //Marker.SetActive(false);
    }

    public void InvokeMissions()
    {
        Markers[CurrentMission].SetActive(true);
        Marker.SetActive(true);
        backButton.SetActive(true);
        MainScreen.SetActive(false);
        MissionScreen.SetActive(true);
        backButton.SetActive(true);
    }

    public void OnClickButton(int MissionNum)
    {
        CurrentMission = MissionNum;
        PlaceMissionUI();
        Invoke("InvokeMissions", 3);
        //InstantiateObject(MissionNum);
    }

    public void UnflipTile()
    {
        Tiles[CurrentMission].GetComponent<Animator>().SetTrigger("back");
    }

    public void InstantiateObject(int mission)
    {
        Instantiate(Markers[mission]);
    }

    public void PlaceMissionUI()
    {
        GKCCounter = 0;
        MissionNumber.text = MissionScripts[CurrentMission].Missionnumber.ToString();
        Instructions.text = MissionScripts[CurrentMission].Instruction;
        Question.text = MissionScripts[CurrentMission].QuestionFinal;
        GKCButtonText.text = MissionScripts[CurrentMission].GKCButtonTextBefore;
        Hint.text = MissionScripts[CurrentMission].HintBoxText;
    }

    public void StartActivity()
    {
        if (GKCCounter == 0)
        {
            this.gameObject.GetComponent<Score>().StartAndEndTimer();
            Invoke("ShowQR", 2);
        }

        GKCButtonText.text = MissionScripts[CurrentMission].GKCButtonTextAfter;
        
    }

    public void ShowQR()
    {
        QR.SetActive(true);
        //Marker.SetActive(true);
    }

    public void MarkerIsScanned()
    {
        
        RateGame.SetActive(true);
        Marker.SetActive(false);
        Markers[CurrentMission].SetActive(false);

    }

    public void Rated()
    {
        this.gameObject.GetComponent<Score>().StartAndEndTimer();
        this.gameObject.GetComponent<Score>().ResetTimer();
        RateGame.SetActive(false);
        Markers[CurrentMission].SetActive(false);
        backButton.SetActive(false);
    }

    public void SubmitAnswer()
    {
        if(AnswerBox.text == MissionScripts[CurrentMission].FinalQuestionAnswers[0].ToString())
        {
            Result.text = "Correct";
            Invoke("AnswerIsCorrect",1);
            this.gameObject.GetComponent<Score>().communicationCounter++;
            this.gameObject.GetComponent<Score>().triesPerQuestion++;
           
        }

        else
        {
            Result.text = "Try Again";
            this.gameObject.GetComponent<Score>().triesPerQuestion++;
        }
    }

    public void AnswerIsCorrect()
    {
        MissionScreen.SetActive(false);
        MainScreen.SetActive(true);
        this.GetComponent<RemoveBlock>().RemoveButton(CurrentMission);
        Result.gameObject.SetActive(false);
        FinalQuestion.SetActive(false);
        QuestionBox.SetActive(false);
        InstructionsBox.SetActive(true);
        GKCButton.SetActive(true);
        backButton.SetActive(true);
        GKCButton.GetComponent<Button>().interactable = true;
    }


}
