﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Mission UI", menuName = "Mission UI")]
public class MissionScript : ScriptableObject
{
    public int Missionnumber;
    //public string QuestionInitial;
    public string QuestionFinal;
    public string Instruction;
    public string GKCButtonTextBefore = "Start Gate Keeper's Quest";
    public string GKCButtonTextAfter = "Scan QR if done";
    public string HintBoxText;
    public string[] FinalQuestionAnswers;
    public bool QRScanned;
    public string Marker;

}
