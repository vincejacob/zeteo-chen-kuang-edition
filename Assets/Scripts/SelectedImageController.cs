﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace App
{
    public class SelectedImageController : MonoBehaviour
    {

        public GameObject ScriptHolder;
        //public GameObject[] Markers;
        public string CurrentImageName;
        public Text test;

        public int[] x;

        private void Start()
        {
            x[0] = 0;
            x[1] = 0;
            x[2] = 0;
            x[3] = 0;
            x[4] = 0;
            x[5] = 0;
            x[6] = 0;
            x[7] = 0;
            x[8] = 0;
            x[9] = 0;
            x[10] = 0;
            x[11] = 0;
            x[12] = 0;
            x[13] = 0;
            x[14] = 0;
            x[15] = 0;
            x[16] = 0;
            x[17] = 0;
            x[18] = 0;
            x[19] = 0;
        }

        // Update is called once per frame
        void Update()
        {
            CurrentImageName = Zeteo.detectedImage;
            test.text = CurrentImageName;
            switch (CurrentImageName)
            {
                case "Alice":
                    if (x[0] == 0)
                    {
                        ScriptHolder.GetComponent<MissionController>().MarkerIsScanned();
                        x[0]++;
                        this.gameObject.GetComponent<Rating>().onClickRate5Star();
                        //Destroy(Markers[0]);
                    }

                    break;

                case "alucard":
                    if (x[1] == 0)
                    {
                        ScriptHolder.GetComponent<MissionController>().MarkerIsScanned();
                        x[1]++; this.gameObject.GetComponent<Rating>().onClickRate5Star();
                        //Destroy(Markers[1]);
                    }
                    break;

                case "estes":
                    if (x[2] == 0)
                    {
                        ScriptHolder.GetComponent<MissionController>().MarkerIsScanned();
                        x[2]++; this.gameObject.GetComponent<Rating>().onClickRate5Star();
                        //Destroy(Markers[2]);
                    }
                    break;

                case "eudora":
                    if (x[3] == 0)
                    {
                        ScriptHolder.GetComponent<MissionController>().MarkerIsScanned();
                        x[3]++; this.gameObject.GetComponent<Rating>().onClickRate5Star();
                        //Destroy(Markers[3]);
                    }
                    break;

                case "fanny":
                    if (x[4] == 0)
                    {
                        ScriptHolder.GetComponent<MissionController>().MarkerIsScanned();
                        x[4]++; this.gameObject.GetComponent<Rating>().onClickRate5Star();
                        //Destroy(Markers[4]);
                    }
                    break;

                case "franco":
                    if (x[5] == 0)
                    {
                        ScriptHolder.GetComponent<MissionController>().MarkerIsScanned();
                        x[5]++; this.gameObject.GetComponent<Rating>().onClickRate5Star();
                        //Destroy(Markers[5]);
                    }
                    break;

                case "gatotkaca":
                    if (x[6] == 0)
                    {
                        ScriptHolder.GetComponent<MissionController>().MarkerIsScanned();
                        x[6]++; this.gameObject.GetComponent<Rating>().onClickRate5Star();
                        //Destroy(Markers[6]);
                    }
                    break;

                case "Gord":
                    if (x[7] == 0)
                    {
                        ScriptHolder.GetComponent<MissionController>().MarkerIsScanned();
                        x[7]++; this.gameObject.GetComponent<Rating>().onClickRate5Star();
                        //Destroy(Markers[7]);
                    }
                    break;

                case "karina":
                    if (x[8] == 0)
                    {
                        ScriptHolder.GetComponent<MissionController>().MarkerIsScanned();
                        x[8]++; this.gameObject.GetComponent<Rating>().onClickRate5Star();
                        //Destroy(Markers[8]);
                    }
                    break;

                case "lancelot":
                    if (x[9] == 0)
                    {
                        ScriptHolder.GetComponent<MissionController>().MarkerIsScanned();
                        x[9]++; this.gameObject.GetComponent<Rating>().onClickRate5Star();
                        //Destroy(Markers[9]);
                    }
                    break;

                case "lapulapu":
                    if (x[10] == 0)
                    {
                        ScriptHolder.GetComponent<MissionController>().MarkerIsScanned();
                        x[10]++; this.gameObject.GetComponent<Rating>().onClickRate5Star();
                        //Destroy(Markers[10]);
                    }
                    break;

                case "layla":
                    if (x[11] == 0)
                    {
                        ScriptHolder.GetComponent<MissionController>().MarkerIsScanned();
                        x[011]++; this.gameObject.GetComponent<Rating>().onClickRate5Star();
                        //Destroy(Markers[11]);
                    }
                    break;

                case "lesley":
                    if (x[012] == 0)
                    {
                        ScriptHolder.GetComponent<MissionController>().MarkerIsScanned();
                        x[012]++; this.gameObject.GetComponent<Rating>().onClickRate5Star();
                        //Destroy(Markers[12]);
                    }
                    break;

                case "minotaur":
                    if (x[013] == 0)
                    {
                        ScriptHolder.GetComponent<MissionController>().MarkerIsScanned();
                        x[013]++; this.gameObject.GetComponent<Rating>().onClickRate5Star();
                        //Destroy(Markers[13]);
                    }
                    break;

                case "miya":
                    if (x[014] == 0)
                    {
                        ScriptHolder.GetComponent<MissionController>().MarkerIsScanned();
                        x[014]++; this.gameObject.GetComponent<Rating>().onClickRate5Star();
                        //Destroy(Markers[14]);
                    }
                    break;

                case "nana":
                    if (x[015] == 0)
                    {
                        ScriptHolder.GetComponent<MissionController>().MarkerIsScanned();
                        x[015]++; this.gameObject.GetComponent<Rating>().onClickRate5Star();
                        //Destroy(Markers[15]);
                    }
                    break;

                case "rafaela":
                    if (x[016] == 0)
                    {
                        ScriptHolder.GetComponent<MissionController>().MarkerIsScanned();
                        x[016]++; this.gameObject.GetComponent<Rating>().onClickRate5Star();
                        //Destroy(Markers[16]);
                    }
                    break;

                case "sun":
                    if (x[017] == 0)
                    {
                        ScriptHolder.GetComponent<MissionController>().MarkerIsScanned();
                        x[017]++; this.gameObject.GetComponent<Rating>().onClickRate5Star();
                        //Destroy(Markers[17]);
                    }
                    break;

                case "yisunshin":
                    if (x[018] == 0)
                    {
                        ScriptHolder.GetComponent<MissionController>().MarkerIsScanned();
                        x[018]++; this.gameObject.GetComponent<Rating>().onClickRate5Star();
                        //Destroy(Markers[18]);
                    }
                    break;

                case "zilong":
                    if (x[019] == 0)
                    {
                        ScriptHolder.GetComponent<MissionController>().MarkerIsScanned();
                        x[019]++; this.gameObject.GetComponent<Rating>().onClickRate5Star();
                        //Destroy(Markers[19]);
                    }
                    break;

                default:
                    Debug.Log("Switch Default");
                    break;
            }
        }


    }

}
