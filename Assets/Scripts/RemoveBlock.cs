﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RemoveBlock : MonoBehaviour
{
    public GameObject[] Button;
    public GameObject MainMenu;
    public Texture[] ImagePool;
    public GameObject ImageToGuess;
    public int PixelLevel = 0;
    

    public void RemoveButton(int ButtonNumber)
    {
        Destroy(Button[ButtonNumber]);
        PixelLevel++;
    }

    public void HideMenu()
    {
        MainMenu.gameObject.SetActive(false);
    }

    public void ShowMenu()
    {
        MainMenu.gameObject.SetActive(true);
    }

    private void Update()
    {
        if (PixelLevel >= 2)
        {
            this.gameObject.GetComponent<Score>().communication = 100;
            ImageToGuess.GetComponent<RawImage>().texture = ImagePool[1];
        }
        if (PixelLevel >= 4)
        {
            this.gameObject.GetComponent<Score>().communication = 95;
            ImageToGuess.GetComponent<RawImage>().texture = ImagePool[2];
        }
        if (PixelLevel >= 6)
        {
            this.gameObject.GetComponent<Score>().communication = 90;
            ImageToGuess.GetComponent<RawImage>().texture = ImagePool[3];
        }
        if (PixelLevel >= 8)
        {
            this.gameObject.GetComponent<Score>().communication = 85;
            ImageToGuess.GetComponent<RawImage>().texture = ImagePool[4];
        }
        if (PixelLevel >= 10)
        {
            this.gameObject.GetComponent<Score>().communication = 80;
            ImageToGuess.GetComponent<RawImage>().texture = ImagePool[5];
        }
        if (PixelLevel >= 12)
        {
            this.gameObject.GetComponent<Score>().communication = 75;
            ImageToGuess.GetComponent<RawImage>().texture = ImagePool[6];
        }
        if (PixelLevel >= 14)
        {
            this.gameObject.GetComponent<Score>().communication = 70;
            ImageToGuess.GetComponent<RawImage>().texture = ImagePool[7];
        }
        if (PixelLevel >= 16)
        {
            this.gameObject.GetComponent<Score>().communication = 65;
            ImageToGuess.GetComponent<RawImage>().texture = ImagePool[8];
        }
        if (PixelLevel >= 18)
        {
            this.gameObject.GetComponent<Score>().communication = 60;
            ImageToGuess.GetComponent<RawImage>().texture = ImagePool[9];
        }
       else
        {
            //ImageToGuess = ImagePool[0];
        }
    }
}
