﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public int speed = 0;
    public int accuracy = 100;
    public int collaboration = 0;
    public int Finalcollaboration;
    public int numberOfRates;
    public int communication = 0;
    public int Finalcommunication;
    public int communicationCounter;
    public int triesPerQuestion;
    public int finalSocre;
    public float timer = 0;
    public bool TimerHasStarted = false;

    public Text SpeedText;
    public Text CollabText;
    public Text CommText;
    public Text AccText;
    public Text FinalScore;

    public GameObject ScoreBoard;
    public GameObject MainScreen;
    public GameObject MissionScreen;

    int AllspeedCounter;

    public void CalculateScore()
    {
        ComputeCollab();
        ComputeSpeed();
        computeCommunication();
        finalSocre = (speed + accuracy + Finalcollaboration + Finalcommunication) / 4;

        SpeedText.text = "Speed: " + speed;
        CollabText.text = "Collaboration: " + Finalcollaboration;
        CommText.text = "Communication: " + Finalcommunication;
        AccText.text = "Accuracy: " + accuracy;
        FinalScore.text = "Final Score: " + finalSocre;
        ScoreBoard.SetActive(true);
        MainScreen.SetActive(false);
        MissionScreen.SetActive(false);
    }

    public void computeCommunication()
    {
        communication = triesPerQuestion / communicationCounter;
        if (communication >= 1)
        {
            Finalcommunication = 100;
        }
        if (communication >= 2)
        {
            Finalcommunication = 90;
        }
        if (communication >= 3)
        {
            Finalcommunication = 70;
        }
        if (communication >= 4)
        {
            Finalcommunication = 50;
        }
        if (communication >= 5)
        {
            Finalcommunication = 30;
        }
    }

    public void StartAndEndTimer()
    {
        TimerHasStarted = !TimerHasStarted;
    }

    public void ResetTimer()
    {
        computePreSpeed();
        
        timer = 0;
        AllspeedCounter++;
    }

    public void ResetTimerOnBack()
    {
        TimerHasStarted = false;
        timer = 0;
        
    }

    public void MinusAcc()
    {
        accuracy -= 10;
    }

    public void ComputeSpeed()
    {
        speed /= AllspeedCounter;
    }

    public void ComputeCollab()
    {
        collaboration /= numberOfRates;

        if (collaboration >= 5)
        {
            Finalcollaboration = 100;
        }
        if (collaboration < 5)
        {
            Finalcollaboration = 90;
        }
        if (collaboration < 4)
        {
            Finalcollaboration = 80;
        }
        if (collaboration < 3)
        {
            Finalcollaboration = 60;
        }
        if (collaboration < 2)
        {
            Finalcollaboration = 50;
        }
        if (collaboration <= 1)
        {
            Finalcollaboration = 30;
        }
    }

    private void Update()
    {
        if (TimerHasStarted == true)
        {
            timer++;
        }
        
    }

    void computePreSpeed()
    {
        int preSpeed =0;
        if (timer <= 14400)
        {
            preSpeed = 30;
        }
        if (timer <= 10800)
        {
            preSpeed = 50;
        }
       
        if (timer <= 7200)
        {
            preSpeed = 75;
        }
        
        if (timer <= 3600)
        {
            preSpeed = 100;
        }
        speed += preSpeed;
       
    }
}
