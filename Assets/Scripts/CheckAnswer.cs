﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckAnswer : MonoBehaviour
{
    [SerializeField]
    //bool correct = false;
    public InputField TextBox;
    public string CorrectAnswer;

    public void CheckAnswerButton()
    {
        
        if (TextBox.text == CorrectAnswer)
        {
            //correct = true;
            Debug.Log("correct");
            this.gameObject.GetComponent<Score>().CalculateScore();
        }
        else
        {
            //correct = false;
            Debug.Log("wrong");
            this.gameObject.GetComponent<Score>().MinusAcc();
        }
    }
}
